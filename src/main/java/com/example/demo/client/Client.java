package com.example.demo.client;

import com.example.demo.passenger.Passenger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@Component
public class Client implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public Client(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(String... args) throws Exception {
      /*  Passenger passenger = this.applicationContext.getBean("passenger", Passenger.class);
        passenger.travel("Koramangala " , " Airport");*/

        //input -> value, output -> boolean
        Predicate<String> stringPredicate = (input) ->  input.contains("spring");

        //input -> value, output -> void
        Consumer<String> strConsumer = (s) ->  System.out.println(s.toUpperCase());

        //input -> value, output -> value
        Function<String, Integer> strLengthFunction = (value) -> value.length();

        //input -> no, output -> value
        Supplier<Integer> supplier = () -> (int)(Math.random() * 10_000);



        /*boolean output = stringPredicate.test("boot");
        System.out.println("Does input string contains 'spring' "+ output);
        */

        strConsumer.accept("spring-boot-training");
        Integer result = strLengthFunction.apply("spring-boot-training");
        System.out.println("Length of the string is "+ result);

        System.out.println("Random integer "+ supplier.get());

    }
}
