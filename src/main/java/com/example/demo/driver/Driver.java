package com.example.demo.driver;

public interface Driver {

    void commute(String from, String destination);
}
