package com.example.demo.driver;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class UberPrime implements Driver {
    @Override
    public void commute(String from, String destination) {
        System.out.println("Commuting from "+from + " to "+ destination + " with Uber Prime");
    }
}
