package com.example.demo.passenger;

import com.example.demo.driver.Driver;
import org.springframework.stereotype.Component;

@Component
public class Passenger {

    private final Driver driver;

    public Passenger(Driver driver) {
        this.driver = driver;
    }

    public void travel(String from, String destination){
        this.driver.commute(from, destination);
    }
}
